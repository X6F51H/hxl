#ifndef EVAL_H
#define EVAL_H

#include <sys/mman.h>

#include "lparse.h"

struct tableS:P {
#define TABLEROWS 9999
#define TABLECOLS (27*27*27)
// null termination excluded
#define CELLENGTH 31

    using cell_t=char[CELLENGTH+1];
    using table_p=cell_t (*)[TABLEROWS][TABLECOLS];

    auto& operator()(table_p p) {
        return (*p)[y][x];
    }
    auto& operator()(table_p p,int y,int x) {
        return (*p)[this->y=y][this->x=x];
    }
};

struct context {
    tableS::table_p H=[]()->tableS::table_p {
        auto r=static_cast<tableS::table_p>(
                mmap(0,sizeof(std::remove_pointer_t<tableS::table_p>),
                    PROT_READ|PROT_WRITE,MAP_PRIVATE|MAP_ANONYMOUS,
                    -1,0)
                );
        assert(!errno&&"zero table mmap failed.");
        return r;
    }();
};

std::string run(parser&&,context&);

#endif
