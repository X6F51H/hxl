#include <cstdlib>
#include <cstdio>

#include <unistd.h>
#include <fcntl.h>

#include "win.h"
#include "eval.h"

std::vector<winS*> winS::windows;

// TODO: Temporarily made a global context. Remove it later.
context con;

struct sheet:winS {
    static constexpr int collen=10;

    tableS ul,lr,sel;

    sheet():winS({},{},{2,5},&sheet::update_) {}

    void lookat(tableS c) {
        assert(ul.y<=c.y&&ul.x<=c.x&&lr.y>=c.y&&lr.x>=c.x
                &&"Outside of visible sheet.");

        y=c.y-ul.y;
        x=(collen+1)*(c.x-ul.x);
        sel=c;
    }
    bool move(P dv) {
        using oadd=intsup::overflow<intsup::ADD>::doit<unsigned>;
        tableS c;
        if(oadd(sel.y,dv.y,&c.y).r) {
            dv.y-=c.y;
            c.y=0;
        }
        if(oadd(sel.x,dv.x,&c.x).r) {
            dv.x-=c.x;
            c.x=0;
        }
        assert(c.y<TABLEROWS&&c.x<TABLECOLS&&"Out of table area.");

        if(ul.y>c.y||ul.x>c.x||lr.y<c.y||lr.x<c.x) {
            lr.y+=dv.y;
            if(oadd(ul.y,dv.y,&ul.y).r) {
                lr.y-=ul.y;
                c.y-=ul.y;
                ul.y=0;
            }
            lr.x+=dv.x;
            if(oadd(ul.x,dv.x,&ul.x).r) {
                lr.x-=ul.x;
                c.x-=ul.x;
                ul.x=0;
            }
        }

        lookat(c);
        return true;
    }

    void draw() {
        // line labels
        attron(A_UNDERLINE|A_BOLD|A_STANDOUT);
        for(int y=ul.y;y<=lr.y;++y)
        mvprintw(y-ul.y+off.y,0,"%4d",y);
        attroff(A_UNDERLINE|A_BOLD|A_STANDOUT);
        // column labels
        for(int x=off.x,i=ul.x;off.x+x<=size.x;x+=(collen+1),++i)
        mvprintw(0,x,"%10s",colnum(i));
        mvchgat(0,0,-1,A_BOLD|A_STANDOUT,0,0);

        // print cells
        tableS sav=sel;
        for(int y=ul.y;y<=lr.y;++y) {
            for(int x=ul.x;x<=lr.x;++x) {
                lookat({y,x});
                wcell();
                wclrtoeol(winres());
            }
        }

        // cell borders
        for(int i=0;i<=lr.y-ul.y;++i)
        mvwchgat(winres(),i,0,-1,A_UNDERLINE,0,0);
        for(int i=collen;i<=(collen+1)*(lr.x-ul.x+1);i+=collen+1)
        mvwvline(winres(),0,i,'|',INT_MAX);

        // fully print selected cell
        wattron(winres(),A_BOLD|A_UNDERLINE);
        lookat(sav);
        wcell();
        wattroff(winres(),A_BOLD|A_UNDERLINE);

        // merge borders with other window
        mvaddch(off.y+size.y,off.x+size.x,ACS_SBSS|A_STANDOUT|A_BOLD);
        mvaddch(off.y+size.y,off.x-1,ACS_SSBS|A_STANDOUT|A_BOLD);
    }

    void wcell() {
        mvwaddstr(winres(),y,x,sel(con.H));
    }
    void rcell() {
        int read=0;

        echo();
        mvwgetnstr(winres(),y,x,sel(con.H),CELLENGTH);
        noecho();
    }

    void update_(int e) {
        switch(e) {
        case '\f':
            wclear(winres());
        break;
        case KEY_RESIZE:
            size=P{LINES/2-off.y-1,COLS-off.x-1};
            x=y=0;
            ul=sel;
            lr={ul.y+size.y-1,ul.x+(size.x-1)/(collen+1)};
            assert(lr.y<TABLEROWS&&lr.x<TABLECOLS
                    &&"bottom-right onscreen-cell is too lower-right.");
            return;
        case '\n':
            rcell();
        break;
        case 'J':
        case KEY_SF:
            move({size.y,0});
        break;
        case 'j':
        case KEY_DOWN:
            move({1,0});
        break;
        case 'K':
        case KEY_SR:
            move({-size.y,0});
        break;
        case 'k':
        case KEY_UP:
            move({-1,0});
        break;
        case 'H':
        case KEY_SLEFT:
            move({0,-size.x/(collen+1)-1});
        break;
        case 'h':
        case KEY_LEFT:
            move({0,-1});
        break;
        case 'L':
        case KEY_SRIGHT:
            move({0,size.x/(collen+1)+1});
        break;
        case 'l':
        case KEY_RIGHT:
            move({0,1});
        break;
        }
        draw();
    }

};
static sheet sheet;

int main(int argc,char *argv[],char *envp[]) {
    for(int i=1,row=0;i<argc;++i) {
        std::FILE *in
            =(argv[i][1]=='\0'&&argv[i][0]=='-')
            ?(clearerr(stdin),stdin)
            :std::fopen(argv[i],"r");
        if(in==nullptr) {
            std::fprintf(stderr,"Warning: failed to open '%s': %s\n",
                         argv[i],std::strerror(errno));
            continue;
        }

        for(int col=0;;) {
            unsigned char c;
            while(std::isspace(c=std::fgetc(in))&&c!='\n');
            if(c=='\n') {
                ++row;
                col=0;
                continue;
            }

            if(feof(in))break;

            std::ungetc(c,in);
            std::fscanf(in,"%" MKSTR(CELLENGTH) "s",sheet.sel(con.H,row,col++));
        }

        if(in!=stdin)fclose(in);
    }
    if(feof(stdin))
        freopen("/dev/stderr","r",stdin);

    initscr();
    std::atexit(reinterpret_cast<void(*)()>(endwin));

    // set default state
    //raw();
    cbreak();
    noecho();
    keypad(stdscr,true);
    //if(curs_set(2)==ERR)exit(1);

    for(int e=KEY_RESIZE,cw=0;e!=EOF&e!='q';e=getch()) {
        switch(e) {
        case '\t':
            cw=(cw+1)%winS::windows.size();
        break;
        case '\f':
            clear();
        case KEY_RESIZE:
            for(auto *w:winS::windows)
                w->resize();
        break;
        default:
            winS::windows[cw]->update(e);
        }

        /* Refresh virtual screen for every window
         * using wnoutrefresh in order from back to front,
         * then update terminal at once using doupdate.
         */
        wnoutrefresh(stdscr);
        for(auto *w:winS::windows)
            w!=winS::windows[cw]&&wnoutrefresh(w->winres());
        touchwin(winS::windows[cw]->winres());
        wnoutrefresh(winS::windows[cw]->winres());
        doupdate();
    }
}
