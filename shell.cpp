#include "win.h"
#include "lparse.h"
#include "eval.h"

struct shell:winS {
    static constexpr std::uint8_t sbs=255;

    std::deque<std::string> sb;
    std::string cmdln;
    std::uint8_t sbp=-1;

    shell():winS{{1,1},{},{},&shell::update_},sb(sbs) {
        cmdln.reserve(LINE_MAX);
    }
    void update_(int);
};
static shell shell;

void shell::update_(int e) {
    switch(e) {
    case '\f':
        scrollok(winres(),true);
        idlok(winres(),true);
        keypad(winres(),true);
        wclear(winres());
    break;
    case KEY_RESIZE:
        size=P{(LINES+1)/2-1,COLS-2};
        off=P{LINES/2,1};
        y=size.y-1;
        return;
    case 'i':
    case '\n':
        char buf[LINE_MAX];
        echo();
        wgetstr(winres(),buf);
        noecho();
        sb.pop_back();
        sb.emplace_front(cmdln+buf);
        cmdln.resize(0);
        sbp=-1;
        // TODO: Temporarily made a global context. Remove it later.
        extern context con;
        wprintw(winres(),run(parser{sb.front().data()},con).c_str());
    break;
    case KEY_LEFT:
    case 'h':
    case 'k':
    case KEY_UP:
        ++sbp;
        cmdln=sb[sbp];
    break;
    case KEY_RIGHT:
    case 'l':
    case 'j':
    case KEY_DOWN:
        --sbp;
        cmdln=sb[sbp];
    break;
    default: return;
    }

    scroll(winres());
    mvwprintw(winres(),y,1,"|-> %s",cmdln.c_str());
    x=5+cmdln.size();

    // merge borders with other window
    mvaddch(off.y-1,off.x+size.x,ACS_SBSS|A_STANDOUT|A_BOLD);
}
