#ifndef WIN_H
#define WIN_H

#include <ncurses.h>

struct winS:P {
    static std::vector<winS*> windows;

    P size,off;
    void (winS::*const update_)(int);
    WINDOW *w=nullptr;

    template<typename T>
    winS(P yx,P s,P o,T f):P{yx},size{s},off{o},
                           update_{static_cast<decltype(update_)>(f)} {
        windows.push_back(this);
    }

    WINDOW* winres() {
        if(!w) {
            w=newwin(size.y,size.x,off.y,off.x);
            assert(w&&"NULL newwin. Possible use before initscr?");
        }
        return w;
    }

    void resize() {
        P osize=size,ooff=off;
        (this->*update_)(KEY_RESIZE);
        int ec;
        if(size!=osize) {
            ec=wresize(winres(),size.y,size.x);
            assert(ec!=ERR&&"win resize failed.");
        }
        if(off!=ooff) {
            ec=mvwin(winres(),off.y,off.x);
            assert(ec!=ERR&&"moved outside of screen.");
        }
        update('\f');
    }

    void update(int e) {
        /* Paint borders but unlike wborder print to the stdscr,
         * just outside of current window.
         */
        mvhline(off.y-1,off.x,ACS_BSBS|A_STANDOUT|A_BOLD,size.x);
        mvvline(off.y,off.x-1,ACS_SBSB|A_STANDOUT|A_BOLD,size.y);
        mvvline(off.y,off.x+size.x,ACS_SBSB|A_STANDOUT|A_BOLD,size.y);
        mvhline(off.y+size.y,off.x,ACS_BSBS|A_STANDOUT|A_BOLD,size.x);
        // border corners
        mvaddch(off.y-1,off.x-1,ACS_BSSB|A_STANDOUT|A_BOLD);
        mvaddch(off.y-1,off.x+size.x,ACS_BBSS|A_STANDOUT|A_BOLD);
        mvaddch(off.y+size.y,off.x-1,ACS_SSBB|A_STANDOUT|A_BOLD);
        mvaddch(off.y+size.y,off.x+size.x,ACS_SBBS|A_STANDOUT|A_BOLD);

        (this->*update_)(e);
        wmove(winres(),y,x);
    }
};

#endif
