#include "eval.h"

/* WTF: meta -> p_op<`something`> -> <`something`>
 * It's overly-complicated machinery to do both compiling and runnig pseudo ops.
 * COMPILING:
 * Every op has const tuple trys. It is a recipe of required tokens or other ops
 * to have this op compiled. collect function template in 'p_op<`op`>' does the
 * trying to get necessary tokens from parser to form new ops. If it succeeds
 * then calls 'tokens' with what it has if 'op' provides a 'tokens' function,
 * otherwise calls empty function template in 'p_op'. This makes collected
 * tokens and other pseudo ops available to newly formed 'op'. This 'op' can
 * store whatever info is needed to run itself later in it.
 * RUNNING:
 * This step is easy. Just iterates over ops stored in vsovec(variable sized
 * objects vector) calling operate member function pointer, derived from meta
 * struct, to do its magic.
 */

namespace {

template<typename T,typename F,typename... FS>
constexpr decltype(auto) dispatch(T&& retval,F&& f,FS&&... fs) {
    if constexpr(std::is_invocable_v<F,T>)
        return f(std::forward<T>(retval));
    else
        return dispatch(std::forward<T>(retval),std::forward<FS>(fs)...);
}

struct meta {
    using vop_t=std::uint8_t(meta::*)(context&);

    const vop_t operate_=nullptr;

    std::uint8_t voperate(context& con) {
        return (this->*operate_)(con);
    }
};

constexpr bool operator==(meta ls,meta rs) {
    return ls.operate_==rs.operate_;
}
constexpr bool operator!=(meta ls,meta rs) {
    return !(ls==rs);
}

struct vsovec {
    char *begin_=nullptr,*end_=nullptr;
    std::size_t capacity=0;

    vsovec()=default;
    vsovec(vsovec&)=delete;
    constexpr vsovec(vsovec&& r) noexcept {
        begin_=r.begin_;
        end_=r.end_;
        capacity=r.capacity;
        r.begin_=nullptr;
    }
    vsovec& operator=(vsovec&)=delete;
    constexpr vsovec& operator=(vsovec&& r) noexcept {
        begin_=r.begin_;
        end_=r.end_;
        capacity=r.capacity;
        r.begin_=nullptr;
        return *this;
    }
    template<typename T>
    vsovec(T&& m) {
        static_assert(std::is_trivially_destructible_v<std::decay_t<T>>,
                "can't hold T.");
        emplace_back(std::forward<T>(m));
    }

    ~vsovec() noexcept { reserve(0); }

    struct iterator {
        char *ptr;

        constexpr iterator(char *p) noexcept :ptr{p} {}

        constexpr bool operator!=(iterator it) const noexcept {
            return ptr!=it.ptr;
        }

        iterator& next(context& con) noexcept {
            ptr+=std::launder(reinterpret_cast<meta*>(ptr))->voperate(con);
            return *this;
        }
        meta& operator*() const noexcept {
            return *std::launder(reinterpret_cast<meta*>(ptr));
        }
    };

    constexpr iterator begin() const noexcept { return {begin_}; }
    constexpr iterator end() const noexcept { return {end_}; }

    void reserve(std::size_t s) {
        assert(s-1>=end_-begin_&&"reserve: Shrinking vsovec is not supported.");

        if(s==0) {
            std::free(begin_);
            begin_=end_=nullptr;
            capacity=0;
            return;
        }

        int ds=end_-begin_;
        if((begin_=static_cast<char*>(std::realloc(begin_,capacity+s)))
                ==nullptr)
            throw std::bad_alloc{};

        capacity+=s;
        end_=begin_+ds;
    }

    template<typename T>
    vsovec& emplace_back(T&& o) {
        using D=std::decay_t<T>;
        static_assert(std::is_trivially_destructible_v<D>,"can't hold T.");

        int adj=alignof(D)-1;
        adj=-(reinterpret_cast<std::uintptr_t>(end_)&adj)&adj;

        if(end_-begin_+adj+sizeof(D)>capacity)
            reserve(capacity+adj+sizeof(D));

        new(end_+adj) D{std::forward<T>(o)};
        end_+=sizeof(D)+adj;

        return *this;
    }
    template<>
    vsovec& emplace_back<const vsovec&>(const vsovec& v) {
        using D=meta;

        int adj=alignof(D)-1;
        adj=-(reinterpret_cast<std::uintptr_t>(end_)&adj)&adj;

        if(end_-begin_+adj+v.capacity>capacity)
            reserve(capacity+adj+v.capacity);

        std::memcpy(end_+adj,v.begin_,v.end_-v.begin_);
        end_+=adj+v.end_-v.begin_;

        return *this;
    }
    template<>
    vsovec& emplace_back<vsovec>(vsovec&& v) {
        return emplace_back<const vsovec&>(v);
    }
    template<>
    vsovec& emplace_back<vsovec&>(vsovec& v) {
        return emplace_back<const vsovec&>(v);
    }
    template<>
    vsovec& emplace_back<vsovec&&>(vsovec&& v) {
        return emplace_back<const vsovec&>(v);
    }

    meta& front() const noexcept {
        return *std::launder(reinterpret_cast<meta*>(begin_));
    }
};

struct anyof {};

template<typename... T,typename E>
constexpr bool is_tokens(E) { return false; }
template<typename O,typename... A,
         typename=decltype(std::declval<O>().tokens(std::declval<A>()...))>
constexpr bool is_tokens(anyof) { return true; }

template<typename T,typename R=T>
using enable_op_t=std::enable_if_t<std::is_base_of_v<meta,std::decay_t<T>>,R>;

template<typename O>
struct p_op:meta {
    constexpr p_op(vop_t v=static_cast<vop_t>(&O::operate)):meta{v} {}

    std::uint8_t operate(context&) {
        // TODO: "bad op";
        return sizeof(O);
    }

    template<typename... T>
    constexpr void resolv(T&&... p) {
        if constexpr(is_tokens<O,T...>(anyof{}))
            static_cast<O*>(this)->tokens(std::forward<T>(p)...);
    }

    inline struct utoken collect(parser&,std::tuple<>) const;
    template<typename... T>
    O collect(parser& p,std::tuple<T...> vs) const {

        auto all=[this,&p](T... tups)->O {
            parser cp=p;
            bool success=true;

            // give to a lamda to extend rvalues lifetime in forward_as_tuple.
            return [&p,&cp,&success](auto&& rts) {
                if(success) {
                    p=cp;
                    return std::apply([](auto&&... toks) {
                            O ret;
                            ret.resolv(std::forward<decltype(toks)>(toks)...);
                            return ret;
                        },rts);
                }
                return O{nullptr};
            }(std::forward_as_tuple(
              dispatch(collect(cp,tups),
                 [&success](token t) { success&=t!=token::ERROR; return t; },
                 [&success](auto m)->enable_op_t<std::decay_t<decltype(m)>> {
                     success&=m!=meta{};
                     return m;
                 },
                 [&success](vsovec v) { success&=v.front()!=meta{}; return v; }
              )...
            ));

        };

        return std::apply(all,vs);
    }
    template<typename... T>
    vsovec collect(parser& p,std::tuple<anyof,T...> vs) const {

        auto any=[this,&p](anyof,T... tups)->vsovec {
            parser cp;
            vsovec ret;
            if(
                ((dispatch(collect(cp=p,tups),
                       [&ret](token t) {
                           bool q=t!=token::ERROR;
                           O r;
                           if(q) {
                               r.resolv(t);
                               ret.emplace_back(r);
                           }
                           return q;
                       },
                       [&ret](auto m)->enable_op_t<decltype(m),bool> {
                           bool q=m!=meta{};
                           if(q)ret.emplace_back(m);
                           return q;
                       },
                       [&ret](vsovec v) {
                           bool q=v.front()!=meta{};
                           if(q)ret.emplace_back(v);
                           return q;
                       })
                 )||...)
              ) {
                p=cp;
            }

            if(ret.capacity==0)
                ret.emplace_back(meta{});
            return ret;
        };

        return std::apply(any,vs);
    }
    template<typename T>
    T collect(parser& p,T t) const {
        parser cp=p;
        return dispatch(t.collect(cp,T::trys),
                [&p,&cp](token t) {
                    if(t!=token::ERROR) {
                        p=cp;
                        T ret;
                        ret.resolv(t);
                        return ret;
                    }
                    return T{nullptr};
                },
                [&p,&cp](T m) {
                    if(m!=meta{}) {
                        p=cp;
                        return m;
                    }
                    return T{nullptr};
                },
                [&p,&cp](vsovec v) {
                    if(v.front()!=meta{}) {
                        p=cp;
                        // TODO: keep correct type
                        return static_cast<T&>(v.front());
                    }
                    return T{nullptr};
                });
    }
    token collect(parser& p,token t) const {
        parser cp=p;
        cp.parse();
        if(t==cp.st.back()) {
            p=cp;
            return cp.st.back();
        }
        return token{"",token::ERROR};
    }
    token collect(parser& p,token::token_type tt) const {
        parser cp=p;
        cp.parse();
        if(cp.st.back()==tt) {
            p=cp;
            return cp.st.back();
        }
        return token{"",token::ERROR};
    }
};

struct value:p_op<value> {
    static constexpr std::tuple trys{anyof{},
        std::tuple{token::BINDEX,token::YX,token::EINDEX,token::IDENT},
        token::NUMBER,
        token::IDENT
    };

    tableS::cell_t imm;
    tableS sel;
    int table;

    void tokens(token id,token yx,token,token) {
        std::string yxs{yx.val};
        auto [xs,ec]=std::from_chars(yxs.data(),yxs.data()+yxs.size(),sel.y);
        assert(!bool(ec)&&"from_chars error. This should not happen.");
        sel.x=colnum(const_cast<char*>(xs));
        table=0; // TODO: id.val;
    }
    void tokens(token t) {
        if(t.type==token::NUMBER) {
            sel={TABLEROWS-1,TABLECOLS-1};
            table=-1;
            std::copy(t.val.begin(),t.val.end(),imm);
            imm[t.val.size()]=0;
        }
        else {
            // TODO: support more than one variable
            table=0;
        }
    }

    std::uint8_t operate(context& con) {
        auto& vc=table==-1?imm:sel(con.H);
        std::copy_n(vc,CELLENGTH+1,sel(con.H,TABLEROWS-1,TABLECOLS-1));

        return sizeof(value);
    }
};

struct arith:p_op<arith> {
    static constexpr std::tuple trys{value{},token::OPERATOR,value{}};

    value l,r;
    char o;

    void tokens(value& lv,token t,value& rv) {
        new(&l) value{lv};
        new(&r) value{rv};
        o=t.val[0];
    }

    std::uint8_t operate(context& con) {
        int x=0,y=0;

        l.operate(con);
        std::from_chars(l.sel(con.H),l.sel(con.H)+CELLENGTH+1,x);
        r.operate(con);
        std::from_chars(r.sel(con.H),r.sel(con.H)+CELLENGTH+1,y);

        tableS sel={TABLEROWS-1,TABLECOLS-1};
        switch(o) {
        case '+':
            *std::to_chars(sel(con.H),sel(con.H)+CELLENGTH+1,x+y).ptr=0;
        break;
        case '-':
            *std::to_chars(sel(con.H),sel(con.H)+CELLENGTH+1,x-y).ptr=0;
        break;
        case '*':
            *std::to_chars(sel(con.H),sel(con.H)+CELLENGTH+1,x*y).ptr=0;
        break;
        case '/':
            *std::to_chars(sel(con.H),sel(con.H)+CELLENGTH+1,x/y).ptr=0;
        break;
        }

        return sizeof(arith);
    }
};

struct assign:p_op<assign> {
    static constexpr std::tuple trys{
        std::tuple{anyof{},arith{},value{}},token::ASSIGN,
        value{}
    };

    std::aligned_union_t<1,arith,value> l;
    value r;

    void tokens(vsovec& lv,token,value& rv) {
        if(lv.end_-lv.begin_==sizeof(value))
            new(&l) value{static_cast<value&>(lv.front())};
        else
            new(&l) arith{static_cast<arith&>(lv.front())};
        new(&r) value{rv};
    }

    std::uint8_t operate(context& con) {
        std::launder(reinterpret_cast<meta*>(&l))->voperate(con);

        std::copy_n(tableS{}(con.H,TABLEROWS-1,TABLECOLS-1),CELLENGTH+1,
                    r.sel(con.H));

        return sizeof(assign);
    }
};

struct debug:p_op<debug> {
    static constexpr std::tuple trys{
        token::BCOMB,value{},token::ECOMB,token{"debug",token::IDENT}
    };
    static std::string log;

    value val;

    void tokens(token,value v,token,token) {
        new(&val) value{v};
    }

    std::uint8_t operate(context& con) {
        val.operate(con);
        log+=val.sel(con.H);
        return sizeof(debug);
    }
};
std::string debug::log;

struct utoken:p_op<utoken> {
    static constexpr std::tuple trys{};

    std::string_view utok;

    void tokens(token t) {
        utok=t.val;
    }

    std::uint8_t operate(context& con) {
        debug::log+="<unexpected token: ";
        debug::log+=utok;
        debug::log+=">";
        return sizeof(utoken);
    }
};
template<typename O>
inline utoken p_op<O>::collect(parser& p,std::tuple<>) const {
    p.parse();
    utoken ret;
    ret.resolv(p.st.back());
    return ret;
}

constexpr std::tuple compile_trys{
    assign{},
    debug{},
    value{},
    utoken{}
};
vsovec compile(parser& p) {
    vsovec ops;
    auto select=[&p,&ops](auto&& m) {
        parser cp=p;
        auto r=m.collect(cp,m);

        if(dispatch(r,
                [](auto o)->enable_op_t<decltype(m),bool> { return o!=meta{}; },
                [](vsovec& v) { return v.front()!=meta{}; })
          ) {
            p=cp;
            ops.emplace_back(r);
            return true;
        }
        return false;
    };

    while(*p.src) {
        std::apply(
            [&p,&select](auto&&... tups) { (select(tups)||...); },compile_trys);
    }

    return ops;
}

} // namespace

std::string run(parser&& p,context& con) {
    debug::log.resize(0);

    vsovec ops=compile(p);
    for(vsovec::iterator pc=ops.begin();pc!=ops.end();)
        pc.next(con);

    return debug::log;
}

