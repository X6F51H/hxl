CPPFLAGS_RELEASE:=$(CPPFLAGS) -DNDEBUG
CXXFLAGS_RELEASE:=$(CXXFLAGS) -std=c++17 -Winvalid-pch -O3
LDFLAGS_RELEASE:=$(LDFLAGS)
CPPFLAGS_DEBUG:=$(CPPFLAGS)
CXXFLAGS_DEBUG:=$(CXXFLAGS) -std=c++17 -Winvalid-pch -Og -g -fsanitize=address,undefined
LDFLAGS_DEBUG:=$(filter-out -s -S --strip-all --strip-debug,$(LDFLAGS))

LIBFLAGS=$(shell pkg-config --cflags ncurses)
LIBS=$(shell pkg-config --libs ncurses)

SRCS:=$(wildcard *.cpp)
OBJS_RELEASE:=$(SRCS:.cpp=.o)
OBJS_DEBUG:=$(SRCS:.cpp=-debug.o)

.PHONY: all clean
all: txl
debug: txl-debug
clean:
	rm -rf *.o *.gch txl txl-debug

common.h.gch: common.h
	$(CXX) $(CPPFLAGS_RELEASE) $(CXXFLAGS_RELEASE) $(LIBFLAGS) -xc++-header -o $@ $<
common-debug.h.gch: common.h
	$(CXX) $(CPPFLAGS_DEBUG) $(CXXFLAGS_DEBUG) $(LIBFLAGS) -xc++-header -o $@ $<

txl: $(OBJS_RELEASE)
	$(CXX) $(CXXFLAGS_RELEASE) $(LDFLAGS_RELEASE) $(LIBFLAGS) -o $@ $^ $(LIBS)
txl-debug: $(OBJS_DEBUG)
	$(CXX) $(CXXFLAGS_DEBUG) $(LDFLAGS_DEBUG) $(LIBFLAGS) -o $@ $^ $(LIBS)

.SECONDEXPANSION:
%.o: %.cpp $$(shell sed -n 's/^\#include "\(.*\)"$$$$/\1/p' %.cpp) common.h.gch
	$(CXX) $(CPPFLAGS_RELEASE) $(CXXFLAGS_RELEASE) $(LIBFLAGS) -include common.h -c -o $@ $<
%-debug.o: %.cpp $$(shell sed -n 's/^\#include "\(.*\)"$$$$/\1/p' %.cpp) common-debug.h.gch
	$(CXX) $(CPPFLAGS_DEBUG) $(CXXFLAGS_DEBUG) $(LIBFLAGS) -include common-debug.h -c -o $@ $<
