#ifndef LPARSE_H
#define LPARSE_H

static inline std::string_view lex(const char *srcp[]) {
#define usrc (static_cast<unsigned char>(*src))
    const char*& src=*srcp;

    while(std::isspace(usrc))++src;
    const char *start=src;

    if(usrc&&!std::isalnum(usrc))++src;
    else if(std::isdigit(usrc))
        while(std::isdigit(usrc))++src;
    else
        while(std::isalnum(usrc))++src;

    return {start,std::size_t(src-start)};
#undef usrc
}

struct token {
    std::string_view val;
    enum token_type {
        ERROR,
        NUMBER,
        IDENT,
        YX,
        BLIST,
        ELIST,
        BCOMB,
        ECOMB,
        BINDEX,
        EINDEX,
        BSCOPE,
        ESCOPE,
        OPERATOR,
        ASSIGN,
        EEXPR
    } type;

    constexpr bool operator==(token t) const {
        return type==t.type&&val==t.val;
    }
    constexpr bool operator==(token_type tt) const {
        return type==tt;
    }
    constexpr bool operator!=(token t) const {
        return !(*this==t);
    }
    constexpr bool operator!=(token_type tt) const {
        return !(*this==tt);
    }
};

struct parser {
    using cont=std::deque<token>;

    const char *src;
    cont st;

    void parse();
};

#endif
