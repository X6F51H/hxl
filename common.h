#ifndef MAIN_H
#define MAIN_H

#include <cassert>
#include <climits>
#include <cerrno>
#include <cctype>
#include <cstring>

#include <type_traits>
#include <utility>
#include <limits>
#include <tuple>

#include <charconv>
#include <algorithm>

#include <string>
#include <vector>
#include <deque>
#include <array>
#include <string_view>

#define MKSTREX(x) #x
#define MKSTR(x) MKSTREX(x)

namespace intsup {
    enum op {SUB=-1,MUL,ADD};

#define ADD_OVERFLOW(x,y,s) \
    __builtin_add_overflow((x),(y),(s))
#define MUL_OVERFLOW(x,y,s) \
    __builtin_mul_overflow((x),(y),(s))
#define SUB_OVERFLOW(x,y,s) \
    __builtin_sub_overflow((x),(y),(s))
#define OVERFLOW(o,x,y,s) \
    o##_OVERFLOW((x),(y),(s))

    template<op op>
    struct overflow;
#define INTSUP_OP(OP) \
    template<> \
    struct overflow<OP> { \
        template<typename S> \
        struct doit { \
            bool r; \
            template<typename X,typename Y> \
            constexpr doit(X x,Y y,std::void_t<S> *s) \
                :r{OVERFLOW(OP,x,y,(S*)s)} {} \
        }; \
        template<typename S,typename X,typename Y> \
        doit(X,Y,S*)->doit<S>; \
    };

    INTSUP_OP(ADD)
    INTSUP_OP(MUL)
    INTSUP_OP(SUB)
#undef INTSUP_OP
}

struct P {
    int y=0,x=0;
    constexpr bool operator==(P s) const { return y==s.y&&x==s.x; }
    constexpr bool operator!=(P s) const { return !(*this==s); }
};

static inline char* colnum(int num) {
    // const char *from="0123456789abcdefghijklmnop";
    // const char *to="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static char buf[8];

    const auto [ptr,ec]=std::to_chars(buf,std::end(buf)-1,num,26);
    assert(!bool(ec)&&"to_chars error. This should not happen.");
    *ptr=0;

    for(char *p=buf;p!=ptr;++p) {
        if(*p<'a')*p+='A'-'0';
        else *p+='K'-'a';
    }

    return buf;
}
static inline int colnum(char *str) {
    char *p;
    int r;

    for(p=str;*p;++p) {
        if(*p<'K')*p-='A'-'0';
        else *p-='K'-'a';
    }

    const auto [ptr,ec]=std::from_chars(str,p,r,26);
    assert(!bool(ec)&&"from_chars error. This should not happen.");

    return r;
}

#endif
