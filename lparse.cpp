#include "lparse.h"

void parser::parse() {
    std::string_view ptoken=lex(&src);
    if(ptoken.size()==0) {
        st.push_back({"<end-of-src>",token::ERROR});
        return;
    }

    token::token_type tt;
    switch(ptoken.front()) {
    case '(':
        tt=token::BCOMB;
    break;
    case ')':
        st.push_back({ptoken,token::ECOMB});
        return;

    case '[':
        tt=token::BINDEX;
    break;
    case ']':
        st.push_back({ptoken,token::EINDEX});
        return;

    case '{':
        tt=token::BSCOPE;
    break;
    case '}':
        st.push_back({ptoken,token::ESCOPE});
        return;

    case ';':
        tt=token::EEXPR;
    break;
    case '=':
        tt=token::ASSIGN;
    break;
    case '+': case '-': case '*': case '/':
        tt=token::OPERATOR;
    break;

    case '1': case '2': case '3': case '4': case '5':
    case '6': case '7': case '8': case '9': case '0':
        if(st.size()&&st.back().type!=token::BINDEX) {
number:     tt=token::NUMBER;
            break;
        }

        const char *ahead;
        ahead=src;
        while(*ahead>='A'&&*ahead<='Z')++ahead;
        if(ahead==src)goto number;
        src=ahead;

        ptoken=std::string_view(ptoken.data(),src-ptoken.data());
        st.push_back({ptoken,token::YX});
        return;

    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g':
    case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n':
    case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u':
    case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B':
    case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I':
    case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P':
    case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W':
    case 'X': case 'Y': case 'Z':
        tt=token::IDENT;
    break;

    default:
        st.push_back({ptoken,token::ERROR});
        return;
    }

    st.push_back({ptoken,tt});
}
